package refactoring;

import java.util.*;

public class Refactoring {

    // 1a
    public int increaseInvoiceNumber() {
        return ++invoiceNumber;
    }

    // 1b
    public void addFilledToOrders(List<Order> orderList) {
        for (Order order : orders) {
            if (order.isFilled()) {
                orderList.add(order);
            }
        }
    }

    // 2a
    public void printStatementFor(Long invoiceId) {
        List<InvoiceRow> invoiceRows = dao.getInvoiceRowsFor(invoiceId);

        printInvoiceRows(invoiceRows);

        // calculate invoice total
        
        double total = getTotalInvoice(invoiceRows);
        printValue(total);
    }
    
    public double getTotalInvoice(List<InvoiceRow> invoiceRows) {
    		double total = 0;
    			for (InvoiceRow invoiceRow : invoiceRows) {
            total += invoiceRow.getAmount();
        }
		return total;
    }

    // 2b
    public String getItemsAsHtml() {
        String result = buildItemAsHtml();
        return result;
    }
    public String buildItemAsHtml() {
    		String result = "";
        result += "<ul>";
        for(String item:items) {
        		result += "<li>" + item + "</li>";
        }
        result += "</ul>";
        return result;
    }

    // 3
    public boolean isSmallOrder() {
        double orderTotal = order.getTotal();
        return (order.getTotal() > 100);
    }

    // 4
    public void printPrice() {

        double VAT = getBasePrice() * 1.2;
        System.out.println("Price not including VAT: " + getBasePrice());
        System.out.println("Price including VAT: " + VAT);
    }

    // 5
    public void calculatePayFor(Job job) {
    		boolean byOur = job.hour > 20 || job.hour < 7;
    		boolean byDay = job.day == 6 || job.day == 7;
        // on holiday at night
        if (byOur && byDay)  {

        }
    }

    // 6
    public boolean canAccessResource(SessionData sessionData) {
        // is admin and has preferred status
        return (hasPreferredStatus(sessionData) && hasPreferredUser(sessionData));
    }
    private boolean hasPreferredStatus(SessionData sessionData) {
    		return sessionData.getStatus().equals("preferredStatusX")
    			           || sessionData.getStatus().equals("preferredStatusY");
    
    }
    private boolean hasPreferredUser(SessionData sessionData) {
    	 return sessionData.getCurrentUserName().equals("Admin")
    			           || sessionData.getCurrentUserName().equals("Administrator");
    }
    // 7
    public void drawLines() {
        Space space = new Space();
        Line line01 = new Line(12, 3, 5, 2, 4, 6);
        Line line02 = new Line(2, 4, 6, 0, 1, 0);
        space.drawLine(line01); //12,3,5= start point, // endpoint
        space.drawLine(line02);
    }

    // 8
    public int calculateWeeklyPayWithOvertime(int hoursWorked) {
        int straightTime = Math.min(40, hoursWorked);
        int straightPay = straightTime * hourRate;
        int overTime = hoursWorked - straightTime;
        double overtimeRate = 1.5 * hourRate;
        int overtimePay = (int) Math.round(overTime * overtimeRate);
        return straightPay + overtimePay;
    }
    public int calculateWeeklyPayWithoutOvertime(int hoursWorked) {
    	    int straightTime = hoursWorked;
    	    int straightPay = straightTime * hourRate;
    	    return straightPay;
    	}

    // //////////////////////////////////////////////////////////////////////////

    // Helper fields and methods.
    // They are here just to make code compile

    private List<String> items = Arrays.asList("1", "2", "3", "4");
    private int hourRate = 5;
    int invoiceNumber = 0;
    private List<Order> orders = new ArrayList<>();
    private Order order = new Order();
    private Dao dao = new SampleDao();
    private double price = 0;

    void justACaller() {
        increaseInvoiceNumber();
        addFilledToOrders(null);
    }

    private void printValue(double total) {
    }

    private void printInvoiceRows(List<InvoiceRow> invoiceRows) {
    }

    class Space {
        public void drawLine(Line line) {
        	
        }
        

    }
    class Line{
    		public Line(int x1, int y1, int z1, int x2, int y2, int z2) {
        	
        }
    }

    interface Dao {
        List<InvoiceRow> getInvoiceRowsFor(Long invoiceId);
    }

    class SampleDao implements Dao {
        @Override
        public List<InvoiceRow> getInvoiceRowsFor(Long invoiceId) {
            return null;
        }
    }

    class Order {
        public boolean isFilled() {
            return false;
        }

        public double getTotal() {
            return 0;
        }
    }

    class InvoiceRow {
        public double getAmount() {
            return 0;
        }
    }

    class Job {
        public int hour;
        public int day;
    }

    private double getBasePrice() {
        return price;
    }

    private class SessionData {

        public String getCurrentUserName() {
            return null;
        }

        public String getStatus() {
            return null;
        }

    }
   //public static void main(String[] args)
    //{}

}
