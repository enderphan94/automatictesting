package rpn;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.ArrayList;

public class RpnCalculatorTest {

    @Test
    public void newCalculatorHasZeroInItsAccumulator() {
        RpnCalculator c = new RpnCalculator();

        assertThat(c.getAccumulator(), is(0));
    }

    @Test
    public void calculatorSupportsAddition() {
        RpnCalculator c = new RpnCalculator();
        c.setAccumulator(1);
        c.enter();
        
        c.setAccumulator(2);
        c.plus();
        
        assertThat(c.getAccumulator(), is(3));
    }
}

class RpnCalculator {

	ArrayList<Integer> array_stack = new ArrayList<Integer>();
	int sum=0;
	int input = 0;

    public Integer getAccumulator() {
        return sum;
    }

	public void plus() {
		for(int i=0; i<=array_stack.size()-1;i++) {
			sum+=array_stack.get(i);
		}
	}

	public void enter() {
		array_stack.add(input);
	}

	public void setAccumulator(int i) {
		if(array_stack.size()<2) {
			input = i;
		}
	}
}