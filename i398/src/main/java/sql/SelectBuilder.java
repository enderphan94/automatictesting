package sql;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

public class SelectBuilder {

	ArrayList<String> arrayColumn = new ArrayList<String>();
	ArrayList<String> arrayTable = new ArrayList<String>();
	ArrayList<String> arrayTable2 = new ArrayList<String>();
	ArrayList<String> arrayWhere = new ArrayList<String>();
	ArrayList<String> arrayCondi = new ArrayList<String>();
	ArrayList<String> arrayValues = new ArrayList<String>();
	ArrayList<Object> arrayPara = new ArrayList<Object>();
	ArrayList<String> arrayId = new ArrayList<String>();
	ArrayList<String> arrayFrom = new ArrayList<String>();
	
	SelectBuilder b ;

	List<Object> objectPara = new ArrayList<Object>();
	Hashtable<String, Object> hashOfNull = new Hashtable<>();
	
	boolean leftJoin = false;
	boolean fromb = false;
    public void column(String column) {
    		arrayColumn.add(column);
    }

    public void from(String table) {
    		arrayTable.add(table);
    }

    public String getSql() {
    		String q="";
    		String joinColumn = String.join(", ", arrayColumn);
    		String joinTable =  String.join(", ", arrayTable);
    		String joinCondi = String.join(" and ", arrayCondi);
    		String joinId = String.join(", ", arrayId);
    		String joinTable2 = String.join(", ", arrayTable2);
    		
    		if(joinCondi.isEmpty()) {
    			 q = "select "+joinColumn+" from "+joinTable;
    		}
    		else {
    			 q = "select "+joinColumn+" from "+joinTable+" where "+ joinCondi;
    		}
    		if(!hashOfNull.isEmpty()) {
    	        for(String key: hashOfNull.keySet()) {
	    	        	arrayValues.add(key+" = ?");
	    	        	String joinValues = String.join(" and ", arrayValues);
	    	        	q = "select "+joinColumn+" from "+joinTable+" where "+ joinValues;
    	        }
    		}
    		
    		if(!arrayId.isEmpty() && !objectPara.isEmpty()) {
    			 q = "select "+joinColumn+" from "+joinTable+" where "+joinId+ " in (?, ?)";
    		}
    		if(leftJoin) {
   			 q = "select "+joinColumn+" from "+joinTable+" left join " +joinTable2+" on "+joinCondi;
    		}
    		
    		if(fromb) {
    			q = "select "+joinColumn+" from " + "("+b.getSql()+")";
    		}
        return q;
    }

    public void columns(String ... columns) {
    		for(int i=0; i<columns.length;i++) {
    			arrayColumn.add(columns[i]);
    		}
    }

    public void where(String condition, Object parameter) {
    		
    		arrayCondi.add(condition);
    		arrayPara.add(parameter);
    		
    		for(int i=0; i<=arrayPara.size()-1;i++) {
    			objectPara.add(arrayPara.get(i));
    		}
    		
    		
    }

    public void where(String condition) {
    		arrayCondi.add(condition);
    }

    public List<Object> getParameters() {
	  
	    for(String keyObject: hashOfNull.keySet()) {
	    		objectPara.add(hashOfNull.get(keyObject));
	    
	    }
        return objectPara;
    }
    public void eqIfNotNull(String column, Object parameter) {
    		if(parameter != null) {
    			hashOfNull.put(column,parameter);
    		}
    		
    }

    public void leftJoin(String table, String condition) {
    		leftJoin = true;
    		arrayTable2.add(table);
    		arrayCondi.add(condition);
    }

    public void in(String column, List<Object> parameters) {
    			arrayId.add(column);
    			objectPara = parameters;
    			
    }

    public void from(SelectBuilder sub) {
    		fromb = true;
    		b =sub;
    }
}