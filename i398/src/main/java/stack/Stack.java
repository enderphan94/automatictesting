package stack;

import java.util.ArrayList;

public class Stack {

	ArrayList<Integer> array_stack = new ArrayList<Integer>();
	
	public Stack(int i) {
		
	}
	public Object getSize() {
		
		return array_stack.size();
	}
	public void push(int a) {
		array_stack.add(a);		
	}

	public int pop() {
		
		int lastPop =0;
		if(array_stack.isEmpty()) {
			throw new IllegalStateException("No element to pop");
		}else {
			lastPop = array_stack.remove(array_stack.size()-1);
		}
		return lastPop;
	}
	
	public int peek() {
		int most_last =0;
		if(array_stack.isEmpty()) {
			throw new IllegalStateException("No element to peek");
		}else {
			most_last = array_stack.get(array_stack.size()-1);
		}	
		return most_last;
	}
	
}
