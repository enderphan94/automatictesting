package stack;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class StackTest {

	@Test
    public void newStackHasNoElements() {
        Stack stack = new Stack(100);
        assertThat(stack.getSize(), is(0));
    }
    
    @Test
    public void stackReturns2Elements() {
        Stack stack = new Stack(100);
        stack.push(2);
        stack.push(3);
        assertThat(stack.getSize(), is(2));
    }
    
    @Test
    public void stackReturns0ElementsAfterPop() {
        Stack stack = new Stack(100);
        stack.push(2);
        stack.push(3);
        stack.pop();
        stack.pop();
        assertThat(stack.getSize(), is(0));
    }
    
    @Test
    public void stackReturnsPushedElements() {
        Stack stack = new Stack(100);
        stack.push(2);
        stack.push(3);
        int pop_1 =  stack.pop();
        int pop_2 = stack.pop();
        assertThat(pop_1, is(3));
        assertThat(pop_2, is(2));
    }
    
    @Test
    public void stackReturnsTopMostElements() {
        Stack stack = new Stack(100);
        stack.push(4);
        stack.push(5);
        int peek_mostlast = stack.peek();
        assertThat(peek_mostlast, is(5));

    }
    
    @Test
    public void stackReturns2ElementsAfterPeeking() {
        Stack stack = new Stack(100);
        stack.push(4);
        stack.push(5);
        stack.peek();
        assertThat(stack.getSize(), is(2));

    }
    
    @Test
    public void stackReturnsIllegalStateExceptionWithPop(){
        Stack stack = new Stack(100);
        assertThrows(IllegalStateException.class, () -> { stack.pop();});
    }
    
    @Test
    public void stackReturnsIllegalStateExceptionWithPeek(){
        Stack stack = new Stack(100);
        assertThrows(IllegalStateException.class, () -> { stack.peek();});
    }

	
}