package stub;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import common.Money;

public class ReportTest {

    @Test
    public void calculatesTotalFromAmounts() {
        Report report = new TestableReport();
        report.setBank(new TestableBank());

        Money total = report.getTotalIncomeBetween(null, null);

        assertThat(total, is(new Money(2, "EUR")));
    }
     
    @Test
    public void calculatesTotalFromAmountsWithAnonymousClass() {
        Report report = new TestableReport() {
        		protected List<Money> getIncomesBetween(Date startDate, Date endDate) {
        			return Arrays.asList(new Money(1, "EUR"), new Money(10, "SEK"));
            }
        };
        report.setBank(new TestableBank(){
	        	public Money convert(Money money, String toCurrency) {
	        		if ("SEK".equals(money.getCurrency())) {
	                    return new Money(money.getAmount() / 10, "EUR");
	                } else {
	                    return money;
	                }
	        	}
        });

        Money total = report.getTotalIncomeBetween(null, null);

        assertThat(total, is(new Money(2, "EUR")));
    }
}
