package stringcalc;

import java.util.Arrays;
import java.util.List;

import org.hamcrest.text.IsEmptyString;

public class StringCalculator {

	public static int add(String text) {
		
		int sum=0;
		List<String> array_text = Arrays.asList(text.split("\\s*,\\s*"));
		
		if(text==null||text.isEmpty()) {
			sum=0;
		}else{		
			for(int i=0;i<array_text.size();i++) {
				
				sum+=Integer.parseInt(array_text.get(i));
			}
		}
		//System.out.println(sum);
		return sum;
	}

	public static void main(String[] args) {
		add("");
	}
}
